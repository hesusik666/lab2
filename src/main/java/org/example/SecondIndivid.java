package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class SecondIndivid {

    private WebDriver chromeDriver;

    private static final String baseUrl = "https://github.com/";

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        //Run driver
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        //set fullscreen
        chromeOptions.addArguments("--start-fullscreen");
        //setup wait for loading elements
        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        this.chromeDriver = new ChromeDriver(chromeOptions);
    }

    @BeforeMethod
    public void preconditions(){
        //open main page
        chromeDriver.get(baseUrl);
    }

    //Successful.
    @Test
    public void testClick(){
        WebElement clickable = chromeDriver.findElement(By.cssSelector("a.btn-mktg.home-campaign-enterprise.btn-muted-mktg"));
        clickable.click();
    }

    //Successful.
    @Test
    public void testEnterData(){

        WebElement search = chromeDriver.findElement(By.cssSelector("input.form-control.js-site-search-focus.header-search-input.jump-to-field.js-jump-to-field"));

        //verification
        Assert.assertNotNull(search);

        //inputValue
        String input = "selenium";
        search.sendKeys(input);

        //verification text
        Assert.assertEquals(search.getAttribute("value"),input);

        //click enter
        search.sendKeys(Keys.ENTER);
    }

    //Successful.
    @Test
    public void testClickWithXPATH(){

        String path = "//button[contains(text(),'Product')]";

        //find element by xpath
        WebElement signInButton = chromeDriver.findElement(By.xpath(path));

        //verification
        Assert.assertNotNull(signInButton);

        signInButton.click();
    }

    //Successful.
    @Test
    public void testCheck(){

        //find element by class name
        WebElement button = chromeDriver.findElement(By.xpath("/html/body/div[1]/div[1]/header/div/div[2]/div/nav/ul/li[2]/button"));

        button.click();

        WebElement buttonEducation = chromeDriver.findElement(By.xpath("/html/body/div[1]/div[1]/header/div/div[2]/div/nav/ul/li[2]/div/ul[1]/li[5]/a"));

        if(buttonEducation.isSelected()){
            buttonEducation.click();
        }
        else{
            System.out.println( String.format("\n Rezult = Education is not selected"));
        }
    }

  /*  @AfterClass(alwaysRun = true)
    public void tearDown(){
        chromeDriver.quit();
    }*/
}
